import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Form } from '@angular/forms';
import { UsuarioService } from '../services/service.index';
import { Usuario } from '../models/usuario.model';

declare function init_plugins();
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {

  public recuerdame: boolean = false;

  constructor(public router: Router,
              public _usuarioService: UsuarioService) { }

  ngOnInit() {
    init_plugins();
  }

  ingresar( forma: any ) {

    console.log('ingresando');
     if (forma.invalid){
       return;
     }

     let usuario = new Usuario(null, forma.value.email, forma.value.password);

     this._usuarioService.loginUsuario(usuario, forma.value.recuerdame)
                          .subscribe( resp => {
                                console.log(resp);
                          });

    //this.router.navigate(['/dashboard']);
  }

}
