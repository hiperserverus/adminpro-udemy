import { Component, OnInit } from '@angular/core';
import { SidebarService } from 'src/app/services/service.index';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: []
})
export class SidebarComponent implements OnInit {
  public menu: any = [];
  constructor(public _sidebar: SidebarService) { }

  ngOnInit() {
    this.cargarMenu();
  }

  cargarMenu() {



    this._sidebar.obtenerMenu()
                        .subscribe((resp: any) => {
                          console.log(resp);
                         
                        });
    }

}
