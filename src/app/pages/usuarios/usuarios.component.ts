import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/usuario.model';
import { UsuarioService } from 'src/app/services/service.index';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styles: []
})
export class UsuariosComponent implements OnInit {

  public usuarios: Usuario[] = [];
  public desde: number = 0;
  public totalRegistros: number = 0;
  public cargando : boolean = true;

  constructor( public _usuarioService : UsuarioService) { }

  ngOnInit() {
    this.cargarUsuarios();
  }

  cargarUsuarios() {

    this.cargando = true;

    this._usuarioService.cargarUsuarios(this.desde)
                        .subscribe((resp: any) => {
                          console.log(resp);
                          this.totalRegistros = resp.total;
                          this.usuarios = resp.usuarios;
                          this.cargando = false;
                        });
    }

    cambiarDesde(valor: number) {

      if ( this.desde >= this.totalRegistros) {
        return;
      }

      if( this.desde <= 0) {
        return;
      }

      this.desde+= valor;
      this.cargarUsuarios();
    }

    buscarUsuario( termino: string) {

      if ( termino.length <= 0 ) {

        this.cargarUsuarios();
        return;
      }

      this.cargando=true;
      
        this._usuarioService.buscarUsuario(termino)
                            .subscribe((usuarios: Usuario[]) => {
                              console.log(usuarios);
                            // this.usuarios = usuarios;
                              this.cargando=false;
                            });
    }

  }



