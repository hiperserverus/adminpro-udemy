import { Component, OnInit } from '@angular/core';
import { PublicacionService } from 'src/app/services/service.index';
import { Publicacion } from '../../models/publicacion.model';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: []
})
export class DashboardComponent implements OnInit {
  public usuarioLoged: string = "5d235965bbbcde16bc111e91";
  public tituloPublicacion: string = "";
  public textoPublicacion: string = "";
  public laspublicaciones: Publicacion[] = [];
  
  forma: FormGroup;

  constructor(public _publicacionService : PublicacionService) { }

  ngOnInit() {
    this.cargarPublicaciones();

    this.forma = new  FormGroup({
      usuario: new FormControl(null),
      titulo: new FormControl(null ),
      texto: new FormControl(null),
      archivo: new FormControl(null)
      

    });
  }

  cargarPublicaciones() {


    this._publicacionService.obtenerPublicaciones()
                        .subscribe((resp: any) => {
                          
                          this.laspublicaciones = resp.publicaciones;
                          console.log(this.laspublicaciones);
                         
                        });
    }

    registrarPublicacion(f) {
    this.forma = f;
  
    let publi = new Publicacion(
      
      this.forma.value.titulo,
      this.forma.value.texto,
      this.forma.value.archivo,
      this.usuarioLoged
    );
  
        this._publicacionService.crearPublicacion( publi )
                          .subscribe( resp => {
                           console.log(resp);
                           this.cargarPublicaciones();
                          })
    }

    onSubmit(f) {
      console.log(f.value.titulo);
      console.log(f.value.texto);
      console.log(f.value.archivo);

    }

}
