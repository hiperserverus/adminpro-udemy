import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { PagesComponent } from './pages.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { FacturasComponent } from './facturas/facturas.component';

import { UsuariosComponent } from './usuarios/usuarios.component';


const pages_routes: Routes = [
  { 
    path: '', 
    component: PagesComponent,
    children: [
      { path: 'dashboard', component: DashboardComponent, data: { titulo: 'Dashboard' } },
      { path: 'facturas', component: FacturasComponent, data: {  titulo:  'Facturas'}},
      {path: 'usuarios', component: UsuariosComponent, data: {titulo: 'Usuarios'}},
      { path: 'account-settings', component: AccountSettingsComponent, data: { titulo: 'Ajustes del tema' } },

      { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    ]
  }
];

export const PAGES_ROUTES = RouterModule.forChild(pages_routes);
