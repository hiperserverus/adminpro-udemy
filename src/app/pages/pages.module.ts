import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';

// Componentes
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { FacturasComponent } from './facturas/facturas.component';
import { UsuariosComponent } from './usuarios/usuarios.component';


// Modulos requeridos
import { FormsModule } from "@angular/forms";
import { ChartsModule } from 'ng2-charts';

// Modulo secundarios
import { SharedModule } from "../shared/shared.module";

// Rutas
import { PAGES_ROUTES } from './pages.routes';



@NgModule({
    declarations: [
        PagesComponent,
        DashboardComponent,
        AccountSettingsComponent,
        FacturasComponent,
        UsuariosComponent
    ],
    exports: [
        PagesComponent,
        DashboardComponent
    ],
    imports: [
        SharedModule,
        FormsModule,
        ChartsModule,
        PAGES_ROUTES,
        CommonModule
    ]
})

export class PagesModule {}