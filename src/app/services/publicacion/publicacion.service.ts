import { Injectable } from '@angular/core';
import { Publicacion } from '../../models/publicacion.model';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICES } from '../../config/config';

@Injectable({
  providedIn: 'root'
})
export class PublicacionService {



  constructor(public http: HttpClient) { 
    console.log('Servicio de publicacion listo');
  }

  obtenerPublicaciones () {

    let url = URL_SERVICES + '/publicacion';

    return this.http.get(url);
  }

  crearPublicacion(publicacion: Publicacion) {

    let url = URL_SERVICES + '/publicacion';

    return this.http.post (url, publicacion);
  }



}
