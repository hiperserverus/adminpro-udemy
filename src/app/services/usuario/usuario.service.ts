import { Injectable } from '@angular/core';
import { Usuario } from '../../models/usuario.model';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICES } from '../../config/config';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(
    public http: HttpClient
  ) { 
    console.log('Servicio de usuario listo');
  }

  loginUsuario ( usuario: Usuario, recuerdame: Boolean = false) {

    let url = URL_SERVICES + '/login';

    return this.http.post(url,usuario);
  }

  crearUsuario( usuario: Usuario ) {

      let url = URL_SERVICES + '/usuario';

      return this.http.post (url, usuario);

  }

  cargarUsuarios(desde: number = 0) {
    let url = URL_SERVICES + '/usuario?desde=' + desde;

    return this.http.get(url);
  }

  buscarUsuario( termino: string) {
      
    let url = URL_SERVICES + '/busqueda/coleccion/usuarios/ ' + termino;

    return this.http.get(url);
  //.map((resp : any) => resp.usuarios);

  }
}
