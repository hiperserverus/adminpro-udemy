import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICES } from 'src/app/config/config';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  

  
menu: any = [
    {
      titulo: 'VENTAS',
      icono: 'mdi mdi-gauge',
      submenu: [
        { titulo: 'Facturas', url: '/facturas'},
        { titulo: 'Boletas de Venta', url: '/facturas'},
        { titulo: 'Notas de Credito', url: '/facturas'},
        { titulo: 'Notas de Debito', url: '/facturas'}
      ]
      
    },
    {
      titulo: 'MANTENIMIENTO',
      icono: 'mdi mdi-gauge',
      submenu: [
        { titulo: 'Usuarios', url: '/usuarios'},
   
      ]
      
    }
  ];

  constructor(
    public http: HttpClient
  ) { 
    console.log('Servicio de sidebar listo');
  }

  
  obtenerMenu ( ) {

    let url = URL_SERVICES + '/tipo/subtipos';

    return this.http.get(url);
  }

  
}
