export class Publicacion {

    constructor(
        
        public titulo: string,
        public texto: string,
        public archivo: string,
        public usuario: any
    ) {}
}